package ZadaniaDodatkoweJavaPods;

import java.util.Scanner;

public class WypisanieLiczb4 {
    public static void main(String[] args) {
        Scanner scan= new Scanner(System.in);
        System.out.println("Podaj swoją liczbę ");
        Integer liczba= scan.nextInt();


        for (int i=1; i<liczba; i++){
            if (i%3==0)
                System.out.println("Pif");
            if (i%7==0)
                System.out.println("Paf");
            if (i%3==0 && i%7==0)
                System.out.println("Pif paf");
        }
    }
}
