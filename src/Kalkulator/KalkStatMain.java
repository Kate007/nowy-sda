package Kalkulator;

import java.util.Scanner;

public class KalkStatMain {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę: ");
        int liczba1 = scan.nextInt();
        System.out.println("Podaj drugą liczbę: ");
        int liczba2 = scan.nextInt();

        Scanner scaan = new Scanner(System.in);

        System.out.println("Jakie działanie chcesz wykonać?");
        String dzialanie = scaan.nextLine();

        if (dzialanie.equalsIgnoreCase("dodawanie")) {
            System.out.println("Suma liczb to: " + KalkulatorStatic.suma(liczba1, liczba2));
        }
        else if (dzialanie.equalsIgnoreCase("odejmowanie")) {
            System.out.println("Różnica liczb to: " + KalkulatorStatic.roznica(liczba1, liczba2));
        }
        else {
            System.out.println("Wpisałeś nieprawidłowe działanie");
        }
    }
}




