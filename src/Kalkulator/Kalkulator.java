package Kalkulator;

import java.io.IOException;
import java.io.OptionalDataException;
import java.util.Scanner;



public class Kalkulator {


    private Integer[] tabLiczb;

    public Kalkulator(Integer[] tabLiczb) {
        this.tabLiczb = tabLiczb;
    }

    public int getSum() {
        int sum = 0;
        for (int i : this.tabLiczb) {
            sum += i;
        }
        return sum;
    }

    public int getOdej() {
        int odej = 0;
        for (int i = 1; i < tabLiczb.length; i++) {
            odej -= i;
        }
        return odej;

    }
    public int getIloczyn() {
        int iloczyn= 0;
        for(int i=1; i<tabLiczb.length; i++) {
            iloczyn *= this.tabLiczb[i];
        }
        return iloczyn;
    }
    public int getIloraz() {
        int iloraz=0;
        for (int i=1; i<tabLiczb.length; i++) {
            iloraz= iloraz/tabLiczb[i];
        }
        return iloraz;
    }
    public int getModulo() {
        int modulo=0;
        for (int i=1; i<tabLiczb.length; i++) {
            modulo= modulo%tabLiczb[i];
        }
        return modulo;
    }
}


