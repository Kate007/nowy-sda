public class Punkt2D {
    int x;
    int y;


    public Punkt2D(int x, int y) {
        this.x = x;
        this.y=y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Punkt2D " +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

