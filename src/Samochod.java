

public class Samochod {

    public static void main(String[] args) {
        Samochod samochod = new Samochod();
        samochod.setTyp("sedan");
        samochod.setRokProdukcji(2002);
        samochod.setMarka("Honda");

        Samochod samochod2 = new Samochod();
        samochod2.setRokProdukcji(2005);
        samochod2.setTyp("combi");
        samochod2.setMarka("Opel");


        System.out.println(samochod.getMarka() + " " + samochod.getTyp() + " " + samochod.rokProdukcji);
        System.out.println(samochod2.getMarka() + " " + samochod2.getTyp() + " " + samochod2.rokProdukcji);


        Samochod samochod3 = new Samochod("Audi ", "sedan", 2020);

        System.out.println(samochod3.toString());


    }

   private String marka;
   private String typ;
    private Integer rokProdukcji;
    Samochod() {

    }

    Samochod(String marka, String typ, Integer rokProdukcji) {
        this.marka= marka;
        this.typ= typ;
        this.rokProdukcji= rokProdukcji;

    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setRokProdukcji(Integer rokProdukcji) {
        this.rokProdukcji = rokProdukcji;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getMarka() {
        return marka;
    }

    public String getTyp() {
        return typ;
    }

    public Integer getRokProdukcji() {
        return rokProdukcji;
    }

    @Override
    public String toString() {
        return "Samochod " + marka + " o typie " + typ + " wyprodukowanym w " + rokProdukcji +
                " roku";
    }
}
