public class Człowiek {
    String imie;
    short wiek;
    String nazwisko;
    int wzrost;

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setWiek(short wiek) {
        this.wiek = wiek;
    }


    public void setWzrost(int wzrost) {
        this.wzrost = wzrost;
    }


    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWzrost() {
        return wzrost;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public short getWiek() {
        return wiek;
    }
}
