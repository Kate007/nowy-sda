package Okregi;

import java.text.DecimalFormat;

public class OkregiMetody {

    public static double obwodOkregu(double radius) {

        double obwodOkregu = 2 * Math.PI * radius;

        return obwodOkregu;
    }

    public static double poleOkregu(double radius) {
        return Math.PI * Math.pow(radius, 2);

    }
}

